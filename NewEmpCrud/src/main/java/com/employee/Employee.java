package com.employee;

	  
	public class Employee {  
	private int empid;  
	private String  ename;  
	private double sal;    
	private String  edes;
	private int  age;         
	public Employee(int empid, String ename, double sal, String edes, int age,
			int deptid) {
		super();
		this.empid = empid;
		this.ename = ename;
		this.sal = sal;
		this.edes = edes;
		this.age = age;
		this.deptid = deptid;
	}
	public int getEmpid() {
		return empid;
	}
	public void setEmpid(int empid) {
		this.empid = empid;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public double getSal() {
		return sal;
	}
	public void setSal(double sal) {
		this.sal = sal;
	}
	public String getEdes() {
		return edes;
	}
	public void setEdes(String edes) {
		this.edes = edes;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public int getDeptid() {
		return deptid;
	}
	public void setDeptid(int deptid) {
		this.deptid = deptid;
	}
	private int  deptid; 
	//no-arg and parameterized constructors  
	//getters and setters  
	}  


