package com.mindgate.training.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/hello")
public class HelloWorldController {

	/**
	 * @param args
	 */
	@RequestMapping("/helloworld")
	public ModelAndView helloworld(){
		String message ="hello world";
		return new ModelAndView("hello","message",message);
	}
	
@RequestMapping("/addnewproduct")
public String createNewProduct(Model model){
	model.addAttribute("product", new Product());
	return "addproduct";
}
	@RequestMapping(value="/addnewproduct",method=RequestMethod.POST)
	public String addProduct(@ModelAttribute("product") Product product,BindingResult result,Model model)
	{
		System.out.println(product.getProductName());
		return "redirect:/mvc/hello/hello";
		
	
	}
}
