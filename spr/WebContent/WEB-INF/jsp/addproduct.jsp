<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
	<title>Spring 3 MVC Series - 
	Actor Manager</title>
</head>
<body>
<h2>Actor Manager</h2>
<form:form method="post" modelAttribute="product" >

	<table>
	<tr>
		<td>
		<td><form:input path="productName" /></td>
		<td> <form:errors path="productName"/> </td>
	</tr>
	<tr>
		<td> Price</td>
		<td><form:input path="price" /></td> 
		<td> <form:errors path="price"/> </td>
	</tr>
	
	
		<tr>
		<td colspan="3">
			<input type="submit" value="Add Product"/>
		</td>
		
	</tr>
</table>	

</form:form>
</body>
</html>