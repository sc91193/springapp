package com.employee;  
import java.sql.PreparedStatement;  
import java.sql.SQLException;  
  
import org.springframework.dao.DataAccessException;  
import org.springframework.jdbc.core.JdbcTemplate;  
import org.springframework.jdbc.core.PreparedStatementCallback;  
  

public class EmployeeDao {
private JdbcTemplate jdbcTemplate;

public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
	this.jdbcTemplate = jdbcTemplate;
}

public int saveEmployee(Employee e){
	String query="insert into employee values('"+e.getEmpid()+"','"+e.getEname()+"','"+e.getEdes()+"','"+e.getAge()+"','"+e.getSal()+"','"+e.getDeptid()+"')";
	return jdbcTemplate.update(query);
}
public int updateEmployee(Employee e){
	String query="update employee set name='"+e.getEname()+"',salary='"+e.getSal()+"' where id='"+e.getEmpid()+"' ";
	return jdbcTemplate.update(query);
}
public int deleteEmployee(Employee e){
	String query="delete from employee where id='"+e.getEmpid()+"' ";
	return jdbcTemplate.update(query);
}

}
